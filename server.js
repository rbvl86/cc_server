'use strict';

var express = require('express');
var app = require('express')();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var port = process.env.PORT || 8080;
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var _ = require('lodash');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

app.set('views', './app/views');
app.set('view engine', 'pug');

var dbConf = require('./app/config/db');
mongoose.connect(dbConf.url);

require('./app/config/passport')(passport);
require('./app/config/passport-local')(passport);

// set up express application
app.use(morgan('dev'));
app.use(cookieParser('keyboard cat'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// required for passport
app.use(session({
  secret: 'keyboard cat',
  cookie: { maxAge: 3600000 },
  store: new MongoStore({ mongooseConnection: mongoose.connection }),
  name: 'cherri.cookie'
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(function (req, res, next) {
  var allowedOrigins = [
    'http://localhost:8100',
    'http://localhost:8101',
    'http://localhost:8000',
    'http://0.0.0.0:8100',
    'http://81.65.195.210:8100',
    'http://81.65.195.210',
    'http://192.168.0.11:8100',
    'http://192.168.0.25:8100',
    'http://192.168.0.25:8101',
    'http://192.168.0.14:8100',
    'http://192.168.0.14:8101',
    'http://192.168.0.14:8000'
  ];
  if (_.includes(allowedOrigins, req.headers.origin)) {
    res.header("Access-Control-Allow-Origin", req.headers.origin);
  }
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-AUTHENTICATION, X-IP, Content-Type, Accept, Authorization");
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  next();
});

require('./app/routes.js')(app, passport, io);
require('./app/routes/user')(app, passport);
require('./app/routes/auth')(app, passport, io);
require('./app/routes/conversation')(app, passport);
require('./app/routes/message')(app, passport);
require('./app/routes/apps')(app, passport);

require('./app/rtc/chat.js')(io);

server.listen(port);
console.info("API launched on port %s", port);