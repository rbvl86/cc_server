'use strict';
/**
 * This model might not be used
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var BiddingSchema = new Schema({
    chips: { type: Number },
    current_winner: { type: ObjectId, ref: 'User' },
    type: { type: String, required: true }
});

module.exports = mongoose.model('Bidding', BiddingSchema);