'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var bcrypt = require('bcrypt');

var UserSchema = new Schema({
  // strategies
  local: {
    username: { type: String, unique: true, required: true },
    password: { type: String, required: true },
    email: { type: String, unique: true, required: true }
  },

  apps: {
    facebook: {
      appname: { type: String, default: 'Facebook' },
      id: { type: String, default: '' },
      token: { type: String, default: '' }
    },

    spotify: {
      appname: { type: String, default: 'Spotify' },
      id: { type: String, default: '' },
      token: { type: String, default: '' }
    },

    deezer: {
      appname: { type: String, default: 'Deezer' },
      id: { type: String, default: '' },
      token: { type: String, default: '' }
    },

    instagram: {
      appname: { type: String, default: 'Instagram' },
      id: { type: String, default: '' },
      token: { type: String, default: '' }
    },

    youtube: {
      appname: { type: String, default: 'YouTube' },
      id: { type: String, default: '' },
      token: { type: String, default: '' }
    },

    googleplus: {
      appname: { type: String, default: 'Google+' },
      id: { type: String, default: '' },
      token: { type: String, default: '' }
    },

    steam: {
      appname: { type: String, default: 'Steam' },
      id: { type: String, default: '' },
      token: { type: String, default: '' }
    },
  },

  photos: [{ type: String }],

  location: {
    latitude: { type: Number },
    longitude: { type: Number },
    postcode: { type: String },
    city: { type: String },
    country: { type: String }
  },
  verified: { type: Boolean, default: true },
  premium: { type: Boolean, default: false },
  favs: [{ type: ObjectId, ref: 'User' }],
  birthdate: { type: Date },
  tweet: { type: String, maxlength: 140 },
  gender: { type: String },
  orientation: { type: String },
  age_range: {
    lower: { type: Number },
    upper: { type: Number }
  },
  flaw: { type: String, maxlength: 64 },
  loves: [{ type: ObjectId, ref: 'Tag' }],
  hates: [{ type: ObjectId, ref: 'Tag' }],
  whatifpos: [{ type: String }],
  whatifneg: [{ type: String }],
  phone: { type: String },
  metadata: {
    created_at: { type: Date, default: Date.now() },
    last_seen: Date
  },
});

/**
 * Encrypt password if changed
 */
UserSchema.pre('save', function (next) {
  var user = this;
  if (user.isModified('local.password') || user.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        console.log(err);
        return next(err);
      }
      bcrypt.hash(user.local.password, salt, function (err, hash) {
        if (err) {
          console.log(err);
          return next(err);
        }
        user.local.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function (passw, cb) {
  bcrypt.compare(passw, this.local.password, function (err, isMatch) {
    if (err) {
      return cb(err);
    }
    cb(null, isMatch);
  });
};

module.exports = mongoose.model('User', UserSchema);
