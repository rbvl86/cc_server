'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var MessageSchema = new Schema({
    to: { type: ObjectId, ref: 'User', required: true },
    from: { type: ObjectId, ref: 'User', required: true },
    content: { type: String, required: true },
    sent_at: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('Message', MessageSchema);