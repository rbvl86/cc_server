'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
var bcrypt = require('bcrypt');

var TagSchema = new Schema({
  value: { type: String, required: true, unique: true }
});

module.exports = mongoose.model('Tag', TagSchema);
