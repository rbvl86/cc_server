'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var ConversationSchema = new Schema({
  users: [{ type: ObjectId, ref: 'User' }],
  started_at: { type: Date, default: Date.now() },
  messages: [{ type: ObjectId, ref: 'Message' }]
});

module.exports = mongoose.model('Conversation', ConversationSchema);