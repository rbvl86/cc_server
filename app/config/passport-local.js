var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;

// load up the user model
var User = require('../models/user');
var config = require('./db'); // get db config file

module.exports = function(passport) {
  passport.serializeUser(function(user, done) {
    done(null, user);
  });

  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  var jwtExtractFromAuthHeader = function(req) {
    var token = null;

    if (req && req.headers) {
      token = req.headers.authorization;
    }

    return token;
  }

  var jwtExtractFromURL = function(req) {
    var token = null;

    if (req && req.query) {
      token = req.query.tk;
    }

    return token;
  }

  var opts = {};
  opts.secretOrKey = config.secret;
  opts.jwtFromRequest = ExtractJwt.fromExtractors([jwtExtractFromAuthHeader, jwtExtractFromURL]);

  passport.use(new JwtStrategy(opts, function(jwt_payload, done) {
    User.findOne({_id: jwt_payload._id}, function(err, user) {
      if (err) {
        return done(err, false);
      }
      if (user) {
        done(null, user);
      } else {
        done(null, false);
      }
    });
  }));
};