'use strict';

module.exports = {
  'facebookAuth': {
    'clientID': '1720315114956838',
    'clientSecret': '222070d676b0632a03377b75ae26bf57',
    'callbackURL': 'http://localhost:8080/connect/facebook/cb/'
  },
  'spotifyAuth': {
    'clientID': '45da493398664d4caec6aa5c85055457',
    'clientSecret': 'e09c3df93747472ea972fb73087c95af',
    'callbackURL': 'http://localhost:8080/connect/spotify/cb/'
  },
  'instagramAuth': {
    'clientID': 'bf259ec488724ae085851f81cf9b85bd',
    'clientSecret': '1871416495cf4e9d963bf1f77d5ffd3e',
    'callbackURL': 'http://localhost:8080/connect/instagram/cb/'
  },
  'steamAuth': {
    'returnURL': 'http://localhost:8080/connect/steam/cb/',
    'realm': 'http://localhost:8080',
    'apiKey': '4FDA13009FB372BA2293C470CF9EE07A'
  }
};