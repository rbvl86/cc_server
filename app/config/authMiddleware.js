module.exports = {
  isLoggedIn: function (req, res, next) {
    if (req.isAuthenticated())
      return next();

    console.log('tried accessing a protected route');
    res.redirect('/');
  }
};