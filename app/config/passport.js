var FacebookStrategy = require('passport-facebook').Strategy;
var SpotifyStrategy = require('passport-spotify').Strategy;
var InstagramStrategy = require('passport-instagram').Strategy;
var SteamStrategy = require('passport-steam').Strategy;

var User = require('../models/user');

var configAuth = require('./auth');

module.exports = function (passport) {

  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  /**
   * FACEBOOK STRATEGY
   */

  passport.use(new FacebookStrategy({
    clientID: configAuth.facebookAuth.clientID,
    clientSecret: configAuth.facebookAuth.clientSecret,
    callbackURL: configAuth.facebookAuth.callbackURL,
    passReqToCallback: true,

    profileFields: ['email', 'displayName', 'gender']
  }, function (req, token, refreshToken, profile, done) {
    console.log('FacebookStrategy');
    var user = req.user;

    user.apps.facebook.id = profile.id;
    user.apps.facebook.token = token;

    var query = { _id: user._id };
    console.log(query);

    User.update(query, user, { overwrite: true }, function () {
      return done(null, user);
    });
  }));

  /**
   * SPOTIFY STRATEGY
   */
  passport.use(new SpotifyStrategy({
    clientID: configAuth.spotifyAuth.clientID,
    clientSecret: configAuth.spotifyAuth.clientSecret,
    callbackURL: configAuth.spotifyAuth.callbackURL,
    passReqToCallback: true
  },
    function (req, accessToken, refreshToken, profile, done) {
      var user = req.user;

      user.apps.spotify.id = profile.id;
      user.apps.spotify.token = accessToken;

      var query = { _id: user._id };

      User.update(query, user, { overwrite: true }, function () {
        return done(null, user);
      });
    }));

  /**
   * INSTAGRAM STRATEGY
   */
  passport.use(new InstagramStrategy({
    clientID: configAuth.instagramAuth.clientID,
    clientSecret: configAuth.instagramAuth.clientSecret,
    callbackURL: configAuth.instagramAuth.callbackURL,
    passReqToCallback: true
  },
    function (req, accessToken, refreshToken, profile, done) {
      console.log('InstagramStrategy');
      console.log(profile);
      console.log(req.user);
      var user = req.user;

      user.apps.instagram.id = profile.id;
      user.apps.instagram.token = accessToken;

      var query = { _id: user._id };

      User.update(query, user, { overwrite: true }, function () {
        return done(null, user);
      })
    }));

  /**
   * STEAM STRATEGY
   */
  passport.use(new SteamStrategy({
    returnURL: configAuth.steamAuth.returnURL,
    realm: configAuth.steamAuth.realm,
    apiKey: configAuth.steamAuth.apiKey,
    passReqToCallback: true
  }, function (req, identifier, profile, done) {
    console.log(identifier);
    var user = req.user;

    user.apps.steam.id = profile.id;
    user.apps.steam.token = identifier;

    var query = { _id: user._id };

    User.update(query, user, { overwrite: true }, function () {
      return done(null, user);
    });

  }));
};