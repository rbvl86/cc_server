var dbConfig = require('./config/db');
var jwt = require('jwt-simple');

var User = require('./models/user');

/**
 * These are the unprotected routes
 */
module.exports = function (app, passport, io) {

  app.get('/', function (req, res) {
    res.status(401);
  });

  app.post('/signup', function (req, res) {
    if (!req.body.username || !req.body.password || !req.body.email) {
      res.json({ success: false, msg: 'Please fill all the fields out' });
    } else {
      console.log(req.body);
      var newUser = new User({
        local: {
          username: req.body.username,
          email: req.body.email,
          password: req.body.password
        }
      });
      // save the user
      newUser.save(function (err, user) {
        console.log(newUser);
        if (err) {
          return res.json(err);
        }

        delete user.local.password;
        var token = jwt.encode(user, dbConfig.secret);
        res.json({ success: true, msg: 'Successfully created new user.', user: user, token: token });
      });
    }
  });

  app.post('/login', function (req, res) {
    User.findOne({
      'local.username': req.body.username
    }, function (err, user) {
      if (err) throw err;

      if (!user) {
        res.send({ success: false, msg: 'Authentication failed. User not found.' });
      } else {
        // check if password matches
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (isMatch && !err) {
            // if user is found and password is right create a token
            var token = jwt.encode(user, dbConfig.secret);
            // return the information including token as JSON
            delete user.local.password;
            res.json({ success: true, token: token, user: user });
          } else {
            res.send({ success: false, msg: 'Authentication failed. Wrong password.' });
          }
        });
      }
    });
  });

  // =====================================
  // LOGOUT ==============================
  // =====================================
  app.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/');
  });
};
