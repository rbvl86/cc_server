'use strict';

var authMiddleware = require('../config/authMiddleware');
var Message = require('../models/message');
var Conversation = require('../models/conversation');

module.exports = function(app, passport) {

  app.route('/messages')
    .get(passport.authenticate('jwt', { session: false}), function(req, res) {

    })
    .post(passport.authenticate('jwt', { session: false}), function(req, res) {
      var message = new Message();

      message.from = req.body.message.from;
      message.to = req.body.message.to;
      message.content = req.body.message.content;

      message.save(function(err) {
        if (err) throw err;
      });

      // save message to conversation
      var conversationId = req.body.conversationId;

      Conversation.findByIdAndUpdate(
        conversationId,
        { $push: {'messages': message._id} },
        { safe: true, upsert: true },
        function(err) {
          if (err) throw err
          res.send(message);
        }
      );
    })
};