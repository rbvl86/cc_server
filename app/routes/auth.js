'use strict';

var _ = require('lodash');
var dbConfig = require('../config/db');
var authConfig = require('../config/auth');
var request = require('request');

/**
 * These are the routes for connecting apps to a Cherri account
 * Basically we authorise a connection to the app profile, get the token and id from the APIs and store em
 * Plain and simple, no personal information involved or stored
 */
module.exports = function(app, passport, io) {
  function nocache(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');
    next();
  }

  var users = [];

  var authSocket = io
    .of('/sync')
    .on('connection', function(socket) {
      socket.on('appconnection', function(id) {
        users.push({ socket: socket.id, id: id });
        console.log('appconnection socket new listener');
        console.log(users);
      });

      socket.on('disconnect', function() {
        console.log('sync socket disconnect');
        var index = _.findIndex(users, { socket: socket.id });
        if (index !== -1 && users[index]) {
          users.splice(index, 1);
        }
      })
    });

  function _appSynced(_id, message) {
    console.log('_appSynced looking for user: ' + _id + ' who just connected to '+ message.app);
    
    var user = _.find(users, function(u) {
      return u.id === _id;
    });

    io.of('/sync').to(user.socket).emit('appconnected', message);
    console.log('_appSynced message sent to socket' + user.socket);
  }

  // --- facebook
  app.get('/connect/facebook', passport.authenticate('jwt', { session: true }), nocache, function(req, res, next) {
    passport.authorize('facebook', { display: null, scope: ['email', 'user_friends', 'user_likes', 'user_photos'], session: false})(req, res, next);
  });
  app.get('/connect/facebook/cb', nocache, passport.authorize('facebook', { failureRedirect: '/' }), function(req, res) {
    _appSynced(req.user._id, { app: 'facebook', token: req.user.apps.facebook.token });
    res.render('appconnected', { app: 'facebook' });
  });

  // --- instagram
  /*app.get('/connect/instagram', passport.authenticate('jwt', { session: false }), function(req, res, next) {
    var redirectUri = 'http://localhost:8080/connect/instagram/cb/?tk='+req.query.tk;
    res.redirect('https://api.instagram.com/oauth/authorize/?client_id='+authConfig.instagramAuth.clientID+'&redirect_uri='+redirectUri+'&response_type=code')
  });
  app.get('/connect/instagram/cb', passport.authenticate('jwt', { session: false }), function(req, res) {
    var code = req.query.code;
    var userId = req.user._id;
    var redirectUri = 'http://localhost:8080/connect/instagram/cb/?tk='+req.query.tk;
    var body = {
      client_id: authConfig.instagramAuth.clientID,
      client_secret: authConfig.instagramAuth.clientSecret,
      grant_type: 'authorization_code',
      redirect_uri: redirectUri,
      code: code
    };

    request.post({
      url: 'https://api.instagram.com/oauth/access_token',
      form: body,
      headers: {"Content-type": "application/x-www-form-urlencoded"}
    }, function(error, httpResponse, response) {
      _appSynced(userId, { app: 'instagram', token: '' });
      res.render('appconnected', { app: 'instagram' });
    });
  });*/

  app.get('/connect/instagram', passport.authenticate('jwt', { session: true }), nocache, function(req, res, next) {
    passport.authorize('instagram', { session: false })(req, res, next);
  });
  app.get('/connect/instagram/cb', nocache, passport.authorize('instagram'), function(req, res) {
    console.log('instagram authenticated');
    console.log(req);
    _appSynced(req.user._id, { app: 'instagram', token: req.user.apps.instagram.token });
    res.render('appconnected', { app: 'instagram' });
  })

  // --- spotify
  app.get('/connect/spotify', passport.authenticate('jwt', { session: true }), nocache, passport.authorize('spotify', {
    scope: ['user-follow-read', 'user-library-read', 'user-top-read'],
    showDialog: true
  }), function(req, res) {
    // never called
  });
  app.get('/connect/spotify/cb', nocache, passport.authorize('spotify', {failureRedirect: '/'}), function(req, res) {
    _appSynced(req.user._id, { app: 'spotify', token: req.user.apps.spotify.token });    
    res.render('appconnected', {app: 'spotify'}); 
  });

  // --- itunes

  // --- deezer

  // --- netflix

  // -- steam
  app.get('/connect/steam', passport.authenticate('jwt', { session: true }), passport.authorize('steam'));
  app.get('/connect/steam/cb', passport.authorize('steam', {failureRedirect: '/'}), function(req, res) {
    _appSynced(req.user._id, 'steam');    
    res.render('appconnected', {app: 'steam'});
  });
};