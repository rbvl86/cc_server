'use strict';

var User = require('../models/user.js');
var Conversation = require('../models/conversation');
var authMiddleware = require('../config/authMiddleware');

var _ = require('lodash');

module.exports = function (app, passport) {
  /**
   * Route /users
   *
   */
  app.route('/users')
    .get(passport.authenticate('jwt', { session: false }), function (req, res) {
      User.find({
        // TODO
        // put filters from client here
      }, function (err, users) {
        if (err) throw err;

        res.json(users);
      });
    });
  app.route('/users/:userId')
    .get(passport.authenticate('jwt', { session: false }), function (req, res) {
      User.findById(req.params.userId, function (err, user) {
        err && res.send(err);

        res.json(user);
      });
    })
    .put(passport.authenticate('jwt', { session: false }), function (req, res) {
      User.findById(req.params.userId, function (err, user) {
        err && res.send(err);

        // update user
        console.log('user updated');
      });
    })
    .delete(passport.authenticate('jwt', { session: false }), function (req, res) {
      User.remove({
        _id: req.params.userId
      }, function (err, user) {
        err && res.send(err);

        console.log('user removed');
      });
    });

  app.route('/users/:userId/conversations')
    .get(passport.authenticate('jwt', { session: false }), function (req, res) {
      Conversation
        .find({ users: req.params.userId })
        .populate('messages')
        .populate('users', null, { _id: { $ne: req.params.userId } })
        .exec(function (err, conversations) {
          if (err) throw err;
          res.send(conversations);
        });
    });

  app.route('/users/:userId/favs')
    .get(passport.authenticate('jwt', {session: false}), function(req, res) {
      User
        .find({ _id: req.params.userId })
        .populate('favs')
        .exec(function(err, user) {
          if (err) throw err;
          res.send(user[0].favs || []);
        })
    })
    .post(passport.authenticate('jwt', {session: false}), function(req, res) {
      User.findByIdAndUpdate(
        req.params.userId,
        { $push: {'favs': req.body.favId} },
        { safe: true, upsert: true },
        function(err) {
          if (err) {
            res.send({success: false, message: 'Issue while adding to favs'});
            throw err;
          }
        }
      );
      res.send({success: true, message: 'Successfully added to favs'});
    });

  app.route('/users/:userId/apps')
    .get(passport.authenticate('jwt', { session: false }), function(req, res) {
      User
        .find({ _id: req.params.userId })
        .exec(function(err, user) {
          if(err) throw err;

          res.send(user[0].apps || {});
        })
    });

    
};
