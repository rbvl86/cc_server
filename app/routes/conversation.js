'use strict';

var authMiddleware = require('../config/authMiddleware');
var Conversation = require('../models/conversation.js');

// MV

/**
 * Route conversations
 */

module.exports = function(app, passport) {
  app.route('/conversations')
    .post(passport.authenticate('jwt', { session: false}), function(req, res) {
      var conversation = new Conversation();

      conversation.users = req.body.users;

      conversation.save(function(err) {
        if(err) throw err;

        res.send(conversation);
      })
    });

  app.route('/conversations/:conversationId')
    .get(passport.authenticate('jwt', { session: false}), function(req, res) {
      Conversation
        .findById(req.params.conversationId)
        .populate('users')
        .populate('messages')
        .exec(function(err, conversation) {
          err && res.send(err);

          res.json(conversation);
        });
    })
    .put(passport.authenticate('jwt', { session: false}), function(req, res) {
      Conversation.findById(req.params.conversationId, function(err, conversation) {
        err && res.send(err);

        console.log('conversation updated');
      });
    })
    .delete(passport.authenticate('jwt', { session: false}), function(req, res) {
      Conversation.remove({
        _id: req.params.conversationId
      }, function(err, conversation) {
        err && res.send(err);

        console.log('conversation deleted');
      });
    });
  app.route('/:userId1/:userId2/conversation')
    .get(passport.authenticate('jwt', { session: false}), function(req, res) {
      Conversation
        .findOne({
          $and: [{users: req.params.userId1}, {users: req.params.userId2}]
        })
        .populate('users')
        // .populate('messages')
        .exec(function(err, conversation) {
          if(err) throw err;

          res.json(conversation);
        });
    })
};