'use strict';

var configDb = require('../config/db');
var request = require('request');

module.exports = function(app, passport) {
  app.route('/user/apps/steam/basic')
    .get(passport.authenticate('jwt', { session: false }), function(req, res) {
      console.log(req.query);
      var id = req.query.steamId;
      var url = 'http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=4FDA13009FB372BA2293C470CF9EE07A&steamids='+id+'&format=json';

      request.get(url, function(err, steamRes, steamBody) {
        res.setHeader('Content-Type', 'application/json');
        res.send(steamBody);
      });
    });

    app.route('/user/apps/instagram/basic')
    .get(passport.authenticate('jwt', { session: false }), function(req, res) {
      var access_token = req.query.access_token;
      var url = 'https://api.instagram.com/v1/users/self/?access_token='+access_token;

      request.get(url, function(err, instagramRes, instagramBody) {
        console.log(instagramBody);
        res.setHeader('Content-Type', 'application/json');
        res.send(instagramBody);
      });
    });

    app.route('/user/apps/instagram/full')
    .get(passport.authenticate('jwt', { session: false }), function(req, res) {
      console.log(req.query);
      var access_token = req.query.access_token;
      var url = 'https://api.instagram.com/v1/users/self/media/recent/?count=100&access_token='+access_token;

      request.get(url, function(err, instagramRes, instagramBody) {
        res.setHeader('Content-Type', 'application/json');
        res.send(instagramBody);
      });
    });
    
}