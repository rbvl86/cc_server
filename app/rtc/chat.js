var _ = require('lodash');

module.exports = function(io) {
  var users = [];

  io.of('/chat').on('connection', function(socket) {
    console.log('chat listeners');
    socket.on('login', function(userId, data) {

    });
    socket.on('subscribe to conversation', function(userId) {
      console.log('subscribed to convo');

      users.push({
        userId: userId,
        socket: socket.id
      })
    });

    socket.on('chatMessage', function(userId, message) {
      var currentUser = _.find(users, { socket: socket.id });
      if (!currentUser) {
        console.log('there is no user attached to this socket');
        return;
      }
      var contact = _.find(users, { userId: userId });
      if(contact) {
        console.log('chatMessage: ', message);
        io.of('/chat').to(contact.socket).emit('chatMessageReceived', currentUser.userId, message);
      } else {
        console.log('user %s is not connected', userId);
        // TODO add message to conversation and send push notification/email
      }
    });

    socket.on('videoMessage', function(userId, message){
      console.log(users);
      console.log('::webrtc message::');
      console.log(message);

      var contact = _.find(users, {userId: userId});
      var currentUser = _.find(users, { socket: socket.id });

      // ignore self and empty messages
      if ((userId === currentUser.id) || !message) {
        console.log('same user and contact')
        return;
      }

      if(!contact) {
        console.log('user %s is not here', userId);
        io.of('/chat').to(currentUser.socket).emit('videoMessage', currentUser.userId, JSON.stringify({ "type": "peeroffline" }));
        return;
      }

      if(!currentUser) {
        console.log('Nobody with this socket or user has been disconnected');
        io.of('/chat').to(currentUser.socket).emit('videoMessage', currentUser.userId, JSON.stringify({ "type": "useroffline" }));
        return;
      }

      io.of('/chat').to(contact.socket).emit('videoMessage', currentUser.userId, message);
    });

    socket.on('disconnect', function(){
      console.log('socket disconnection');
      var index = _.findIndex(users, { socket: socket.id });
      if (index !== -1 && users[index]) {
        socket.broadcast.emit('offline', users[index].userId);
        console.log(users[index].userId + ' disconnected');

        users.splice(index, 1);
      }
    });
  });
};